﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroStateBase : StateMachineBehaviour
{
    private PlayerController controller;

    public PlayerController GetPlayerController(Animator animator)
    {
        if (controller == null)
        {
            controller = animator.GetComponentInParent<PlayerController>();
        }
        return controller;
    }
}
