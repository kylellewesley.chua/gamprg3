﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroMove : HeroStateBase
{
    [SerializeField] private float speed;
    

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        GetPlayerController(animator).GetAnimator().SetBool("IsMoving", true);
        GetPlayerController(animator).GetAgent().speed = speed;
        GetPlayerController(animator).GetAgent().isStopped = false;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Vector3 dir;
        dir = CheckIfHasTarget(animator);

        float dot = Vector3.Dot(dir, GetPlayerController(animator).transform.forward);

        //Debug.Log(dot);
        switch (GetPlayerController(animator).GetState())
        {
            case PlayerState.State.Idle:
                animator.SetBool("IsIdle", true);
                animator.SetBool("IsMoving", false);
                break;

            case PlayerState.State.Running:
                if (dot >= .6f)
                {
                    switch (GetPlayerController(animator).hasTarget)
                    {
                        case true:
                            GetPlayerController(animator).GetAgent().stoppingDistance = 5f;
                            GetPlayerController(animator).GetAgent().SetDestination(GetPlayerController(animator).GetTarget().position);

                            if (GetPlayerController(animator).TargetDistanceORLocationDistance() < 6f)
                            {
                                //Attack State;
                                GetPlayerController(animator).SetState(PlayerState.State.Attack);
                               
                            }



                            break;

                        case false:

                            GetPlayerController(animator).GetAgent().stoppingDistance = 0;

                            if (GetPlayerController(animator).CheckDistance())
                            {
                                GetPlayerController(animator).GetAgent().SetDestination(GetPlayerController(animator).moveLocation);
                            }
                            else
                            {
                                GetPlayerController(animator).SetState(PlayerState.State.Idle);
                            }

                            break;
                    }

                    
                }
                else
                {

                    GetPlayerController(animator).IsLookingAtLocation();
                }

                break;

            case PlayerState.State.Attack:
                animator.SetBool("IsAttacking", true);
                animator.SetBool("IsMoving", false);
                break;
        }




    }

   

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        GetPlayerController(animator).GetAnimator().SetBool("IsMoving", false);
    }

    private Vector3 CheckIfHasTarget(Animator animator)
    {
        Vector3 dir;
        if (GetPlayerController(animator).GetTarget() != null)
        {
            dir = (GetPlayerController(animator).GetTarget().position - GetPlayerController(animator).transform.position).normalized;
        }
        else
        {
            dir = (GetPlayerController(animator).moveLocation - GetPlayerController(animator).transform.position).normalized;
        }

        return dir;
    }


}
