﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroIdle : HeroStateBase
{
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        GetPlayerController(animator).GetAgent().isStopped = true;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Vector3 dir = (GetPlayerController(animator).moveLocation - GetPlayerController(animator).transform.position).normalized;
        float dot = Vector3.Dot(dir, GetPlayerController(animator).transform.forward);

        switch (GetPlayerController(animator).GetState())
        {
            case PlayerState.State.Idle:
                //Waiting for Input;
                if (GetPlayerController(animator).hasTarget)
                {
                    GetPlayerController(animator).SetState(PlayerState.State.Running);
                    animator.SetBool("IsIdle", false);
                    animator.SetBool("IsMoving", true);
                }
                else
                {
                    animator.SetBool("IsIdle", true);
                    animator.SetBool("IsMoving", false);
                }

               
                break;

            case PlayerState.State.Running:
                if(dot >= 0.9f)
                {
                    GetPlayerController(animator).GetAgent().isStopped = false;
                    animator.SetBool("IsIdle", false);
                    animator.SetBool("IsMoving", true);
                }
                else
                {
                    GetPlayerController(animator).LookAtLocationLerp();
                }               
                break;
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
       
    }

    
}
