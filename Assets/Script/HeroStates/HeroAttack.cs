﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroAttack : HeroStateBase
{
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //GetPlayerController(animator).SetState(PlayerState.State.Attack);
        GetPlayerController(animator).GetAnimator().SetBool("IsAttacking", true);
        GetPlayerController(animator).GetAgent().isStopped = true;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (GetPlayerController(animator).GetTarget() != null)
        {
            var distance = Vector3.Distance(GetPlayerController(animator).GetTarget().position, GetPlayerController(animator).transform.position);
            GetPlayerController(animator).transform.LookAt(GetPlayerController(animator).GetTarget());
            if (GetPlayerController(animator).hasTarget && distance < 5f)
            {
                GetPlayerController(animator).SetState(PlayerState.State.Attack);

            }
            else
            {
                GetPlayerController(animator).SetState(PlayerState.State.Running);
            }

        }
        else
        {
            GetPlayerController(animator).SetState(PlayerState.State.Running);

        }


        switch (GetPlayerController(animator).GetState())
        {
            case PlayerState.State.Running:
                animator.SetBool("IsAttacking", false);
                animator.SetBool("IsMoving", true);
                break;

            case PlayerState.State.Attack:
                animator.SetBool("IsAttacking", true);
                animator.SetBool("IsMoving", false);
                break;

            case PlayerState.State.Idle:
                animator.SetBool("IsAttacking", false);
                animator.SetBool("IsMoving", false);
                break;
        }

       
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        GetPlayerController(animator).GetAgent().isStopped = false;
        GetPlayerController(animator).GetAnimator().SetBool("IsAttacking", false);
    }

    
}
