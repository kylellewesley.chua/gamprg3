﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraHandler : MonoBehaviour
{

    [SerializeField] float panSpeed = 2f;
    [SerializeField] float zoomSpeed = 3f;
    [SerializeField] float zoomInMax = 10f;
    [SerializeField] float zoomOutMax = 50f;

    private CinemachineInputProvider inputProvider;
    private CinemachineVirtualCamera cinemachineVirtualCamera;
    private Transform cameraTransform;
    
  

   // private float zoomY;

    private void Awake()
    {
        inputProvider = GetComponent<CinemachineInputProvider>();
        cinemachineVirtualCamera = GetComponent<CinemachineVirtualCamera>();
        cameraTransform = cinemachineVirtualCamera.VirtualCameraGameObject.transform;
    }

    // Start is called before the first frame update
    void Start()
    {
        
        //zoomY = cinemachineVirtualCamera.gameObject.transform.position.y;
    }
  

    // Update is called once per frame
    void Update()
    {
        float x = inputProvider.GetAxisValue(0);
        float y = inputProvider.GetAxisValue(1);
        float z = inputProvider.GetAxisValue(2);

        if (x != 0 || y != 0)
        {
            PanScreen(x, y);
        }
        if(z != 0)
        {
            ZoomScreen(z);
        }

        //if (cinemachineVirtualCamera == null)
        //    return;

        

        //CameraZoom();

    }

    public void ZoomScreen(float increment)
    {
        float fov = cinemachineVirtualCamera.m_Lens.FieldOfView;
        float target = Mathf.Clamp(fov + increment, zoomInMax, zoomOutMax);
        cinemachineVirtualCamera.m_Lens.FieldOfView = Mathf.Lerp(fov, target, zoomSpeed * Time.deltaTime);







        //float posY = cameraTransform.position.y;
        //float targetY = Mathf.Clamp(posY + increment, zoomInMax, zoomOutMax);
        //Vector3 cameraPos = new Vector3(cameraTransform.position.x, posY, cameraTransform.position.z);
        //Vector3 newCameraPos = new Vector3(cameraTransform.position.x, posY + targetY , cameraTransform.position.z);

        //cameraTransform.position = Vector3.Lerp(cameraPos, newCameraPos, zoomSpeed * Time.deltaTime);


    }

    public Vector3 PanDirection(float x, float y)
    {

        Vector3 direction = Vector3.zero;
        if (y >= Screen.height * .95f)
        {
            direction.z += 1;
        }
        else if (y <= Screen.height * 0.05f)
        {
            direction.z -= 1;
        }
        if(x >= Screen.width * .95f)
        {
            direction.x += 1;
        }
        else if(x <= Screen.width * 0.05f)
        {
            direction.x -= 1;
        }
        return direction;

    }


    public void PanScreen(float x, float y)
    {
        Vector3 direction = PanDirection(x, y);
       
        cameraTransform.position = Vector3.Lerp(cameraTransform.position, cameraTransform.position + direction * panSpeed, Time.deltaTime);
    }




    //private void CameraZoom()
    //{
    //    float zoomAmount = .1f;
    //    zoomY += Input.mouseScrollDelta.y * (zoomAmount * zoomSpeed);
    //    float minPerspectiveSize = 10;
    //    float maxPerspectiveSize = 50;
    //    zoomY = Mathf.Clamp(zoomY, minPerspectiveSize, maxPerspectiveSize);

    //    cinemachineVirtualCamera.gameObject.transform.position = new Vector3(cinemachineVirtualCamera.gameObject.transform.position.x, 
    //                                                                          zoomY, 
    //                                                                         cinemachineVirtualCamera.gameObject.transform.position.z);
    //}
}
