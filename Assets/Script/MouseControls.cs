// GENERATED AUTOMATICALLY FROM 'Assets/Script/MouseControls.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @MouseControls : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @MouseControls()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""MouseControls"",
    ""maps"": [
        {
            ""name"": ""Mouse"",
            ""id"": ""80b81f00-a6d4-4386-af12-7c1782c9929d"",
            ""actions"": [
                {
                    ""name"": ""Pan"",
                    ""type"": ""PassThrough"",
                    ""id"": ""5858e141-6501-4bb2-ab03-34251c652a08"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Zoom"",
                    ""type"": ""PassThrough"",
                    ""id"": ""d9ac97b7-507b-49e4-b268-a4f4c8995e95"",
                    ""expectedControlType"": ""Axis"",
                    ""processors"": ""Normalize(min=-1,max=1)"",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""3bc40c93-2642-491b-9e0d-d51611be8647"",
                    ""path"": ""<Mouse>/position"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Pan"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c64cb83e-4caa-4b60-a70c-fae9766575bd"",
                    ""path"": ""<Mouse>/scroll/y"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Zoom"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""UnitControl"",
            ""id"": ""6e696d75-b9a6-4533-b8d8-51d513c733b2"",
            ""actions"": [
                {
                    ""name"": ""MovePlayer"",
                    ""type"": ""Button"",
                    ""id"": ""e8845b5b-04ad-46a9-8202-97b396ce1d57"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": ""Normalize(min=-1,max=1)"",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ControlUnit"",
                    ""type"": ""Button"",
                    ""id"": ""982d9444-4c5e-4d39-b8c1-b0412803f763"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": ""Normalize(min=-1,max=1)"",
                    ""interactions"": """"
                },
                {
                    ""name"": ""MousePosition"",
                    ""type"": ""PassThrough"",
                    ""id"": ""935def3f-9004-416c-98ac-738e097334db"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": ""Normalize(min=-1,max=1)"",
                    ""interactions"": """"
                },
                {
                    ""name"": ""StopUnit"",
                    ""type"": ""Button"",
                    ""id"": ""369bdc2f-54c5-49bf-9643-9d67d500c640"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": ""Normalize(min=-1,max=1)"",
                    ""interactions"": """"
                },
                {
                    ""name"": ""1stSkill"",
                    ""type"": ""Button"",
                    ""id"": ""6374e9fd-ebfd-43a4-b69c-7b7e0a51fe98"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": ""Normalize(min=-1,max=1)"",
                    ""interactions"": """"
                },
                {
                    ""name"": ""2ndSkill"",
                    ""type"": ""Button"",
                    ""id"": ""5c24f408-d559-47a7-88c9-a3fb4baeebad"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": ""Normalize(min=-1,max=1)"",
                    ""interactions"": """"
                },
                {
                    ""name"": ""3rdSkill"",
                    ""type"": ""Button"",
                    ""id"": ""8d6e7fdc-8194-4652-ae02-63cc0e0038ff"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": ""Normalize(min=-1,max=1)"",
                    ""interactions"": """"
                },
                {
                    ""name"": ""4thSkill"",
                    ""type"": ""Button"",
                    ""id"": ""53c76742-6971-491f-aa59-76d9f61f9fb5"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": ""Normalize(min=-1,max=1)"",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""a79f6e93-6d87-49af-9ab5-40975def9b0a"",
                    ""path"": ""<Mouse>/rightButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MovePlayer"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""de6f31ed-886c-4603-827c-a49f098b8563"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ControlUnit"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""5437d63d-309d-4284-84d6-b4aa089831cf"",
                    ""path"": ""<Mouse>/position"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MousePosition"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3d73be5b-3341-4883-afa4-173993a052fa"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""StopUnit"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""706c5362-3bfa-4f75-9de0-7009c5d1e87d"",
                    ""path"": ""<Keyboard>/q"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""1stSkill"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e3264084-84bb-4539-b88f-2c27848462da"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""2ndSkill"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""35b1f09f-43f3-48bf-893f-f2bdc6c61f9c"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""3rdSkill"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""df299804-c2ba-4292-a1fb-384892f9eeb4"",
                    ""path"": ""<Keyboard>/r"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""4thSkill"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // Mouse
        m_Mouse = asset.FindActionMap("Mouse", throwIfNotFound: true);
        m_Mouse_Pan = m_Mouse.FindAction("Pan", throwIfNotFound: true);
        m_Mouse_Zoom = m_Mouse.FindAction("Zoom", throwIfNotFound: true);
        // UnitControl
        m_UnitControl = asset.FindActionMap("UnitControl", throwIfNotFound: true);
        m_UnitControl_MovePlayer = m_UnitControl.FindAction("MovePlayer", throwIfNotFound: true);
        m_UnitControl_ControlUnit = m_UnitControl.FindAction("ControlUnit", throwIfNotFound: true);
        m_UnitControl_MousePosition = m_UnitControl.FindAction("MousePosition", throwIfNotFound: true);
        m_UnitControl_StopUnit = m_UnitControl.FindAction("StopUnit", throwIfNotFound: true);
        m_UnitControl__1stSkill = m_UnitControl.FindAction("1stSkill", throwIfNotFound: true);
        m_UnitControl__2ndSkill = m_UnitControl.FindAction("2ndSkill", throwIfNotFound: true);
        m_UnitControl__3rdSkill = m_UnitControl.FindAction("3rdSkill", throwIfNotFound: true);
        m_UnitControl__4thSkill = m_UnitControl.FindAction("4thSkill", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Mouse
    private readonly InputActionMap m_Mouse;
    private IMouseActions m_MouseActionsCallbackInterface;
    private readonly InputAction m_Mouse_Pan;
    private readonly InputAction m_Mouse_Zoom;
    public struct MouseActions
    {
        private @MouseControls m_Wrapper;
        public MouseActions(@MouseControls wrapper) { m_Wrapper = wrapper; }
        public InputAction @Pan => m_Wrapper.m_Mouse_Pan;
        public InputAction @Zoom => m_Wrapper.m_Mouse_Zoom;
        public InputActionMap Get() { return m_Wrapper.m_Mouse; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(MouseActions set) { return set.Get(); }
        public void SetCallbacks(IMouseActions instance)
        {
            if (m_Wrapper.m_MouseActionsCallbackInterface != null)
            {
                @Pan.started -= m_Wrapper.m_MouseActionsCallbackInterface.OnPan;
                @Pan.performed -= m_Wrapper.m_MouseActionsCallbackInterface.OnPan;
                @Pan.canceled -= m_Wrapper.m_MouseActionsCallbackInterface.OnPan;
                @Zoom.started -= m_Wrapper.m_MouseActionsCallbackInterface.OnZoom;
                @Zoom.performed -= m_Wrapper.m_MouseActionsCallbackInterface.OnZoom;
                @Zoom.canceled -= m_Wrapper.m_MouseActionsCallbackInterface.OnZoom;
            }
            m_Wrapper.m_MouseActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Pan.started += instance.OnPan;
                @Pan.performed += instance.OnPan;
                @Pan.canceled += instance.OnPan;
                @Zoom.started += instance.OnZoom;
                @Zoom.performed += instance.OnZoom;
                @Zoom.canceled += instance.OnZoom;
            }
        }
    }
    public MouseActions @Mouse => new MouseActions(this);

    // UnitControl
    private readonly InputActionMap m_UnitControl;
    private IUnitControlActions m_UnitControlActionsCallbackInterface;
    private readonly InputAction m_UnitControl_MovePlayer;
    private readonly InputAction m_UnitControl_ControlUnit;
    private readonly InputAction m_UnitControl_MousePosition;
    private readonly InputAction m_UnitControl_StopUnit;
    private readonly InputAction m_UnitControl__1stSkill;
    private readonly InputAction m_UnitControl__2ndSkill;
    private readonly InputAction m_UnitControl__3rdSkill;
    private readonly InputAction m_UnitControl__4thSkill;
    public struct UnitControlActions
    {
        private @MouseControls m_Wrapper;
        public UnitControlActions(@MouseControls wrapper) { m_Wrapper = wrapper; }
        public InputAction @MovePlayer => m_Wrapper.m_UnitControl_MovePlayer;
        public InputAction @ControlUnit => m_Wrapper.m_UnitControl_ControlUnit;
        public InputAction @MousePosition => m_Wrapper.m_UnitControl_MousePosition;
        public InputAction @StopUnit => m_Wrapper.m_UnitControl_StopUnit;
        public InputAction @_1stSkill => m_Wrapper.m_UnitControl__1stSkill;
        public InputAction @_2ndSkill => m_Wrapper.m_UnitControl__2ndSkill;
        public InputAction @_3rdSkill => m_Wrapper.m_UnitControl__3rdSkill;
        public InputAction @_4thSkill => m_Wrapper.m_UnitControl__4thSkill;
        public InputActionMap Get() { return m_Wrapper.m_UnitControl; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(UnitControlActions set) { return set.Get(); }
        public void SetCallbacks(IUnitControlActions instance)
        {
            if (m_Wrapper.m_UnitControlActionsCallbackInterface != null)
            {
                @MovePlayer.started -= m_Wrapper.m_UnitControlActionsCallbackInterface.OnMovePlayer;
                @MovePlayer.performed -= m_Wrapper.m_UnitControlActionsCallbackInterface.OnMovePlayer;
                @MovePlayer.canceled -= m_Wrapper.m_UnitControlActionsCallbackInterface.OnMovePlayer;
                @ControlUnit.started -= m_Wrapper.m_UnitControlActionsCallbackInterface.OnControlUnit;
                @ControlUnit.performed -= m_Wrapper.m_UnitControlActionsCallbackInterface.OnControlUnit;
                @ControlUnit.canceled -= m_Wrapper.m_UnitControlActionsCallbackInterface.OnControlUnit;
                @MousePosition.started -= m_Wrapper.m_UnitControlActionsCallbackInterface.OnMousePosition;
                @MousePosition.performed -= m_Wrapper.m_UnitControlActionsCallbackInterface.OnMousePosition;
                @MousePosition.canceled -= m_Wrapper.m_UnitControlActionsCallbackInterface.OnMousePosition;
                @StopUnit.started -= m_Wrapper.m_UnitControlActionsCallbackInterface.OnStopUnit;
                @StopUnit.performed -= m_Wrapper.m_UnitControlActionsCallbackInterface.OnStopUnit;
                @StopUnit.canceled -= m_Wrapper.m_UnitControlActionsCallbackInterface.OnStopUnit;
                @_1stSkill.started -= m_Wrapper.m_UnitControlActionsCallbackInterface.On_1stSkill;
                @_1stSkill.performed -= m_Wrapper.m_UnitControlActionsCallbackInterface.On_1stSkill;
                @_1stSkill.canceled -= m_Wrapper.m_UnitControlActionsCallbackInterface.On_1stSkill;
                @_2ndSkill.started -= m_Wrapper.m_UnitControlActionsCallbackInterface.On_2ndSkill;
                @_2ndSkill.performed -= m_Wrapper.m_UnitControlActionsCallbackInterface.On_2ndSkill;
                @_2ndSkill.canceled -= m_Wrapper.m_UnitControlActionsCallbackInterface.On_2ndSkill;
                @_3rdSkill.started -= m_Wrapper.m_UnitControlActionsCallbackInterface.On_3rdSkill;
                @_3rdSkill.performed -= m_Wrapper.m_UnitControlActionsCallbackInterface.On_3rdSkill;
                @_3rdSkill.canceled -= m_Wrapper.m_UnitControlActionsCallbackInterface.On_3rdSkill;
                @_4thSkill.started -= m_Wrapper.m_UnitControlActionsCallbackInterface.On_4thSkill;
                @_4thSkill.performed -= m_Wrapper.m_UnitControlActionsCallbackInterface.On_4thSkill;
                @_4thSkill.canceled -= m_Wrapper.m_UnitControlActionsCallbackInterface.On_4thSkill;
            }
            m_Wrapper.m_UnitControlActionsCallbackInterface = instance;
            if (instance != null)
            {
                @MovePlayer.started += instance.OnMovePlayer;
                @MovePlayer.performed += instance.OnMovePlayer;
                @MovePlayer.canceled += instance.OnMovePlayer;
                @ControlUnit.started += instance.OnControlUnit;
                @ControlUnit.performed += instance.OnControlUnit;
                @ControlUnit.canceled += instance.OnControlUnit;
                @MousePosition.started += instance.OnMousePosition;
                @MousePosition.performed += instance.OnMousePosition;
                @MousePosition.canceled += instance.OnMousePosition;
                @StopUnit.started += instance.OnStopUnit;
                @StopUnit.performed += instance.OnStopUnit;
                @StopUnit.canceled += instance.OnStopUnit;
                @_1stSkill.started += instance.On_1stSkill;
                @_1stSkill.performed += instance.On_1stSkill;
                @_1stSkill.canceled += instance.On_1stSkill;
                @_2ndSkill.started += instance.On_2ndSkill;
                @_2ndSkill.performed += instance.On_2ndSkill;
                @_2ndSkill.canceled += instance.On_2ndSkill;
                @_3rdSkill.started += instance.On_3rdSkill;
                @_3rdSkill.performed += instance.On_3rdSkill;
                @_3rdSkill.canceled += instance.On_3rdSkill;
                @_4thSkill.started += instance.On_4thSkill;
                @_4thSkill.performed += instance.On_4thSkill;
                @_4thSkill.canceled += instance.On_4thSkill;
            }
        }
    }
    public UnitControlActions @UnitControl => new UnitControlActions(this);
    public interface IMouseActions
    {
        void OnPan(InputAction.CallbackContext context);
        void OnZoom(InputAction.CallbackContext context);
    }
    public interface IUnitControlActions
    {
        void OnMovePlayer(InputAction.CallbackContext context);
        void OnControlUnit(InputAction.CallbackContext context);
        void OnMousePosition(InputAction.CallbackContext context);
        void OnStopUnit(InputAction.CallbackContext context);
        void On_1stSkill(InputAction.CallbackContext context);
        void On_2ndSkill(InputAction.CallbackContext context);
        void On_3rdSkill(InputAction.CallbackContext context);
        void On_4thSkill(InputAction.CallbackContext context);
    }
}
