﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class OnBuildingDestroyEvent : UnityEvent<Buildings> { }


[SerializeField]
public class BuildingType
{
    public enum BuildingTypes
    {
        MeleeBarracks,
        RangeBarracks,
        Tower,
        Core
    }
}


public class Buildings : MonoBehaviour
{
    public OnBuildingDestroyEvent OnBuildingDestroy;

    [SerializeField] private FactionType.FactionTypes factionTypes;
    [SerializeField] private BuildingType.BuildingTypes types;
    [SerializeField] private bool isVunerable = false; 
    [SerializeField] private float health = 2000;
    [SerializeField] private float damage = 0;
    [SerializeField] private float shootTimerMax;
    private float shootTimer;
    [SerializeField] private float armor = 10;
    [SerializeField] private float bountyReward = 200f;
    [SerializeField] private float expReward = 50f;
    [SerializeField] private Transform targetTransform;
    private bool hasTarget;
    private float targetMaxRadius = 30;
    

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (types == BuildingType.BuildingTypes.Tower)
        {
            CheckForTarget(targetTransform);
            if (targetTransform == null)
            {
                LookForTarget();
            }
        }
     
       
    }

    private void HandleShooting()
    {
        shootTimer -= Time.deltaTime;
        if(shootTimer <= 0f)
        {
            shootTimer += shootTimerMax;
            if (targetTransform != null)
            {
                if(types == BuildingType.BuildingTypes.Tower)
                {
                    GetComponent<ProjectilePrefab>().GetFirePoint().LookAt(targetTransform);
                    GetComponent<ProjectilePrefab>().FireTowerProjectile();
                }
              
            }
        }
        
    }

    public void CheckForTarget(Transform target)
    {
        if (target != null)
        {
            hasTarget = true;
            HandleShooting();

        }
        else
        {
            hasTarget = false;
            LookForTarget();
        }
    }


    public void LookForTarget()
    {

        Collider[] colliderArray = Physics.OverlapSphere(transform.position, targetMaxRadius);


        foreach (Collider collider in colliderArray)
        {
            Creep creep = collider.GetComponent<Creep>();
            

            CheckCreepsNearby(creep);
           

        }
    }


    private void CheckCreepsNearby(Creep creep)
    {
        if (creep != null)
        {
            //Check if they are not on the same faction
            if (creep.GetFaction() != this.gameObject.GetComponent<Buildings>().GetFactionType())
            {
                if (targetTransform == null)
                {
                    targetTransform = creep.transform;
                }
                else
                {
                    //change to the closest target
                    if (Vector3.Distance(transform.position, creep.transform.position) <
                        Vector3.Distance(transform.position, targetTransform.position))
                    {
                        targetTransform = creep.transform;
                    }
                }
            }
        }
    }


    public FactionType.FactionTypes GetFactionType()
    {
        return factionTypes;
    }

    public BuildingType.BuildingTypes GetBuildingType()
    {
        return types;
    }

    public bool GetIsVunerable()
    {
        return isVunerable;
    }

    public void SetIsVunerable(bool value)
    {
        isVunerable = value;
    }

    public float GetHealth()
    {
        return health;
    }

    public float GetDamage()
    {
        return damage;
    }

    public float GetArmor()
    {
        return armor;
    }

    public float GetBountyReward()
    {
        return Random.Range(bountyReward - 20, bountyReward);
    }

    public float GetExpReward()
    {
        return expReward;
    }

    public Transform GetTargetTransform()
    {
        return targetTransform;
    }

    public void SetTargetTransformToNull()
    {
        targetTransform = null;
    }

    public void OnDie()
    {
        OnBuildingDestroy?.Invoke(this);
        gameObject.SetActive(false);
        Invoke("Destroy", 3f);
    }


    private void Destroy()
    {
        Destroy(this.gameObject);
    }

}
