﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Health : MonoBehaviour
{
    public UnityEvent OnDie;

    [SerializeField] private float healthCurrent;
    [SerializeField] private float healthMax;
    [SerializeField] private float bountyReward;
    [SerializeField] private float expReward;

    public float MyHealthCurrent { get { return healthCurrent; } set { MyHealthCurrent = value; } }
    

    private void Awake()
    {
        if (this.GetComponent<Creep>())
        {
            healthMax = GetComponent<Creep>().GetHealth();
            healthCurrent = healthMax;
            bountyReward = GetComponent<Creep>().GetBountyReward();
            expReward = GetComponent<Creep>().GetExpReward();
        }

        if (this.GetComponent<Buildings>())
        {
            healthMax = GetComponent<Buildings>().GetHealth();
            healthCurrent = healthMax;
            bountyReward = GetComponent<Buildings>().GetBountyReward();
            expReward = GetComponent<Buildings>().GetExpReward();
        }



    }

    // Start is called before the first frame update
    void Start()
    {
        if (this.GetComponent<Stats>())
        {
            healthMax = GetComponent<Stats>().MyHealthMax;
            healthCurrent = healthMax;
            bountyReward = GetComponent<Stats>().MyBountyReward;
            expReward = GetComponent<Stats>().MyExpReward;
            StartCoroutine(ConstantHealthRegen());
        }
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnDamage(float damage, GameObject objectThatDealtDamage)
    {
        healthCurrent -= damage;
        if (healthCurrent <= 0)
        {
            healthCurrent = 0;
            if (objectThatDealtDamage.GetComponent<TestDebugControl>())
            {
                Debug.Log("Player Killed Creep");
                objectThatDealtDamage.GetComponent<TestDebugControl>().bountyEarned += bountyReward;
                objectThatDealtDamage.GetComponent<TestDebugControl>().expEarned += expReward;
            }

            if (objectThatDealtDamage.GetComponent<PlayerController>())
            {
                objectThatDealtDamage.GetComponent<PlayerController>().hasTarget = false;
                objectThatDealtDamage.GetComponent<PlayerController>().SetTargetTransformToNull();
            }
            gameObject.SetActive(false);
            SetCreepTargetToNull(objectThatDealtDamage);
            SetBuildingTargetToNull(objectThatDealtDamage);

            Destroy(gameObject);
            OnDie?.Invoke();


        }
    }

    private IEnumerator ConstantHealthRegen()
    {
        while(healthCurrent > 0)
        {
            HealthRegen();

            yield return new WaitForSeconds(1f);
        }
    }

    private void HealthRegen()
    {
        if(healthCurrent < healthMax)
        {
            healthCurrent += GetComponent<Stats>().MyHpRegen;
        }
        else
        {
            healthCurrent = healthMax;
        }
    }

    public void UpdateHealthMax()
    {
        healthMax = GetComponent<Stats>().MyHealthMax;
    }

   

    private static void SetBuildingTargetToNull(GameObject objectThatDealtDamage)
    {
        if (objectThatDealtDamage.GetComponent<Buildings>())
        {
            objectThatDealtDamage.GetComponent<Buildings>().SetTargetTransformToNull();
        }
    }

    private static void SetCreepTargetToNull(GameObject objectThatDealtDamage)
    {
        if (objectThatDealtDamage.GetComponent<AIController>())
        {
            objectThatDealtDamage.GetComponent<AIController>().SetTargetTransformToNull();
        }
    }



}
