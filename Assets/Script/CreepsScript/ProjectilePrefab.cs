﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectilePrefab : MonoBehaviour
{
    [SerializeField] private GameObject projectile;
    [SerializeField] private Transform firepoint;

  
    public void FireProjectile()
    {
        if(GetComponentInParent<AIController>().GetTargetTransform() != null)
        {
            Projectile.Create(firepoint.position, GetComponentInParent<AIController>().GetTargetTransform(), this.gameObject.transform);
        }
        
    }

    public void FireTowerProjectile()
    {
        if (GetComponent<Buildings>().GetTargetTransform())
        {
            
            TowerProjectile.Create(firepoint.position, GetComponent<Buildings>().GetTargetTransform(), this.gameObject.transform);
        }
    }


    public GameObject GetProjectile()
    {
        return projectile;
    }

    public Transform GetFirePoint()
    {
        return firepoint;
    }
}
