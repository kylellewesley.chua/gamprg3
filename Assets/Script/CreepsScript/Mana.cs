﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mana : MonoBehaviour
{
    [SerializeField] private float manaCurrent;
    [SerializeField] private float manaMax;

    private void Awake()
    {
        if (this.GetComponent<Creep>())
        {
            manaMax = GetComponent<Creep>().GetMana();
            manaCurrent = manaMax;
        }

      
       
    }

    // Start is called before the first frame update
    void Start()
    {
        if (this.GetComponent<Stats>())
        {
            manaCurrent = GetComponent<Stats>().MyMana;
            manaMax = GetComponent<Stats>().MyManaMax;
            StartCoroutine(ConstantManaRegen());
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private IEnumerator ConstantManaRegen()
    {
        while(manaCurrent > 0)
        {
            ManaRegen();
            yield return new WaitForSeconds(1f);
        }
        
    }

    public void UpdateManaMax()
    {
        manaMax = GetComponent<Stats>().MyManaMax;
    }

    private void ManaRegen()
    {
        if(manaCurrent < manaMax)
        {
            manaCurrent += GetComponent<Stats>().MyMpRegen;
        }
        else
        {
            manaCurrent = manaMax;
        }
    }

    public void ManaUsage(float manaCost)
    {
        manaCurrent -= manaCost;
    }

    public float GetCurrentMana()
    {
        return manaCurrent;
    }

}
