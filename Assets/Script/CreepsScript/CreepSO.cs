﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "ScriptableObject/CreepSO")]
public class CreepSO : ScriptableObject
{
    public CreepType.CreepTypes types;
    public float attack = 40f;
    public float health = 550f;
    public float mana = 0f;
    public float armor = 0f;
    public float statGain = 0.3f;
}

