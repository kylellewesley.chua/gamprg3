﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackAnimEvents : MonoBehaviour
{
    [SerializeField] private GameObject weaponMesh;
    

    // Start is called before the first frame update
    void Start()
    {
        weaponMesh.GetComponent<BoxCollider>().enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AttackEnter()
    {
        Debug.Log("WeaponActivate");
        weaponMesh.GetComponent<BoxCollider>().enabled = true;
    }

    public void AttackExit()
    {
        Debug.Log("WeaponDeactivate");
        weaponMesh.GetComponent<BoxCollider>().enabled = false;
    }


}
