﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.AI;

public class AIState
{
    public enum State
    {
        Idle,
        Running,
        Attack
    };
}


public class AIController : MonoBehaviour
{

    [SerializeField] private AIState.State aiState;
    [SerializeField] private NavMeshAgent agent;
    [SerializeField] private Animator anim;
    [SerializeField] private List<Waypoints> waypoints;
    [SerializeField] private float targetMaxRadius = 20f;

    [SerializeField] private Transform targetTransform;
    public bool hasTarget { private set; get; }
    [SerializeField] private float turnRate;

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        
    }

    // Start is called before the first frame update
    void Start()
    {
        WaypointManager waypointManager = SingletonManager.Get<WaypointManager>();

        switch (GetComponent<Creep>().GetFaction() == FactionType.FactionTypes.Radiant)
        {
            case true:
                switch (GetComponent<Creep>().GetLane())
                {

                    case SpawnerLane.SpawnLane.MID:
                        waypoints = waypointManager.GetRadiantMidWaypoints();
                        break;
                    case SpawnerLane.SpawnLane.TOP:
                        waypoints = waypointManager.GetRadiantTopWaypoints();
                        break;
                    case SpawnerLane.SpawnLane.BOT:
                        waypoints = waypointManager.GetRadiantBotWaypoints();
                        break;
                }
                break;

            case false:
                switch (GetComponent<Creep>().GetLane())
                {

                    case SpawnerLane.SpawnLane.MID:
                        waypoints = waypointManager.GetDireMidWaypoints();
                        break;
                    case SpawnerLane.SpawnLane.TOP:
                        waypoints = waypointManager.GetDireTopWaypoints();
                        break;
                    case SpawnerLane.SpawnLane.BOT:
                        waypoints = waypointManager.GetDireBotWaypoints();
                        break;
                }
                break;

        }

        
    }

    // Update is called once per frame
    void Update()
    {
        CheckForTarget(targetTransform);
        if(targetTransform == null)
        {
            LookForTarget();
        }
        
        
    }



    public void CheckForTarget(Transform target)
    {
        if (target != null)
        {
            hasTarget = true;
            Vector3 direction = target.position - transform.position;
            Quaternion rotation = Quaternion.LookRotation(direction);
            transform.rotation = Quaternion.Lerp(transform.rotation, rotation, turnRate * Time.deltaTime);
        }
        else
        {
            hasTarget = false;
            LookForTarget();
        }
    }



    public void LookForTarget()
    {
        
        Collider[] colliderArray = Physics.OverlapSphere(transform.position, targetMaxRadius);
        

        foreach (Collider collider in colliderArray)
        {
            PlayerController enemyPlayer = collider.GetComponent<PlayerController>();
            Creep creep = collider.GetComponent<Creep>();
            Buildings building = collider.GetComponent<Buildings>();

            CheckHeroesNearby(enemyPlayer);
            CheckCreepsNearby(creep);
            CheckBuildingsNearby(building);

        }
    }


    public Transform GetTarget()
    {
        return targetTransform;
    }

    private void CheckBuildingsNearby(Buildings building)
    {
        if (building != null)
        {
            if (building.GetFactionType() != this.gameObject.GetComponent<Creep>().GetFaction())
            {
                if (targetTransform == null)
                {
                    if (building.GetIsVunerable())
                    {
                        targetTransform = building.transform;
                    }
                   
                }
                else
                {
                    //change to the closest target
                    if (Vector3.Distance(transform.position, building.transform.position) <
                    Vector3.Distance(transform.position, targetTransform.position))
                    {
                        targetTransform = building.transform;
                        Debug.Log("Building located: " + targetTransform);
                    }
                }
            }
        }
    }

    private void CheckCreepsNearby(Creep creep)
    {
        if (creep != null)
        {
            //Check if they are not on the same faction
            if (creep.GetFaction() != this.gameObject.GetComponent<Creep>().GetFaction())
            {
                if (targetTransform == null)
                {
                    targetTransform = creep.transform;
                }
                else
                {
                    //change to the closest target
                    if (Vector3.Distance(transform.position, creep.transform.position) <
                        Vector3.Distance(transform.position, targetTransform.position))
                    {
                        targetTransform = creep.transform;
                    }
                }
            }
        }
    }


    private void CheckHeroesNearby(PlayerController enemyPlayer)
    {
        if (enemyPlayer != null)
        {
            //Check if they are not on the same faction
            if (enemyPlayer.GetFaction() != this.gameObject.GetComponent<Creep>().GetFaction())
            {
                if (targetTransform == null)
                {
                    targetTransform = enemyPlayer.transform;
                }
                else
                {
                    //change to the closest target
                    if (Vector3.Distance(transform.position, enemyPlayer.transform.position) <
                        Vector3.Distance(transform.position, targetTransform.position))
                    {
                        targetTransform = enemyPlayer.transform;
                    }
                }
            }
        }
    }

    public AIState.State GetState()
    {
        return aiState;
    }

    public void SetState(AIState.State value)
    {
        aiState = value;
    }

    public NavMeshAgent GetAgent()
    {
        return agent;
    }

    public List<Waypoints> GetWaypoints()
    {
        return waypoints;
    }

    public Animator GetAnimator()
    {
        return anim;
    }

    public Transform GetTargetTransform()
    {
        return targetTransform;
    }

    public void SetTargetTransformToNull()
    {
        targetTransform = null;
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, 20);
    }





}
