﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeAttackTrigger : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (GetComponentInParent<AIController>())
        {
            if (GetComponentInParent<AIController>().GetTargetTransform() != null && other.gameObject == GetComponentInParent<AIController>().GetTargetTransform().gameObject)
            {
                if (GetComponentInParent<Creep>())
                {
                    other.gameObject.GetComponent<Health>().OnDamage(GetComponentInParent<Creep>().GetDamage(), gameObject.GetComponentInParent<Creep>().gameObject);
                }

            }
        }

        if (GetComponentInParent<PlayerController>())
        {

            if (GetComponentInParent<PlayerController>().GetTarget() != null && other.gameObject == GetComponentInParent<PlayerController>().GetTarget().gameObject)
            {
                if (GetComponentInParent<Stats>())
                {
                    other.gameObject.GetComponent<Health>().OnDamage(GetComponentInParent<Stats>().MyDamage, gameObject.GetComponentInParent<Stats>().gameObject);
                }
            }
        }

       

    }


}
