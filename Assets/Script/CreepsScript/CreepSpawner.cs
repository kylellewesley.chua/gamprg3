﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerLane
{
    public enum SpawnLane
    {
        MID,
        TOP,
        BOT
    };
}

public class CreepSpawner : MonoBehaviour
{
    [SerializeField] SpawnerLane.SpawnLane lane;
    [SerializeField] List<Creep> creepsToSpawn;
    [SerializeField] List<Buildings> barracksList;
    [SerializeField] Transform spawnPoint;

    [Header("Spawn Variable")]
    [SerializeField] private int meleeCreepCount;
    [SerializeField] private int rangeCreepCount;
    [SerializeField] private int siegeCreepCount;
    [SerializeField] private int waveCount;
    [SerializeField] private int upgradeCount;
    [SerializeField] private bool meleeDestroyed;
    [SerializeField] private bool rangeDestroyed;
    
    
    

    // Start is called before the first frame update
    void Start()
    {
        SpawnManager spawnManager = SingletonManager.Get<SpawnManager>();
        
        //spawnManager.OnShouldSpawn += SpawnManager_OnShouldSpawn;
        //spawnManager.OnShouldSpawnEvents.AddListener(SpawnManagerOnShouldSpawn);
       
    }

    //private void SpawnManager_OnShouldSpawn(object sender, SpawnManager.OnShouldSpawnArgs e)
    //{
    //   Debug.Log("Spawn Creep");    
    //}



    // Update is called once per frame
    void Update()
    {
       
    }

    public void SpawnCreep()
    {
        waveCount++;
        
        foreach (var creep in creepsToSpawn)
        {
            switch (meleeDestroyed)
            {
                case false:
                    {
                        if (creep.GetCreepType() == CreepType.CreepTypes.Melee && creep.GetCreepPower() == CreepPower.CreepPowers.Normal)
                        {
                            for (int i = 0; i < meleeCreepCount; i++)
                            {

                                if (upgradeCount <= 0)
                                {
                                    
                                    GameObject creepObject = Instantiate(creep.gameObject, spawnPoint.position + RandomPosition(), Quaternion.identity);
                                    creepObject.GetComponent<Creep>().SetLane(lane);
                                }
                                else
                                {
                                    creep.SetUpgradeCount(upgradeCount);
                                    GameObject creepObject = Instantiate(creep.gameObject, spawnPoint.position + RandomPosition(), Quaternion.identity);
                                    creepObject.GetComponent<Creep>().SetLane(lane);
                                    //creepObject.GetComponent<Creep>().SetUpgradeCount(upgradeCount);


                                }


                            }
                        }
                        break;
                    }
                case true:
                    {
                        if (creep.GetCreepType() == CreepType.CreepTypes.Melee && creep.GetCreepPower() == CreepPower.CreepPowers.Super)
                        {
                            for (int i = 0; i < meleeCreepCount; i++)
                            {

                                if (upgradeCount <= 0)
                                {
                                    GameObject creepObject = Instantiate(creep.gameObject, spawnPoint.position + RandomPosition(), Quaternion.identity);
                                    creepObject.GetComponent<Creep>().SetLane(lane);
                                }
                                else
                                {
                                    creep.SetUpgradeCount(upgradeCount);
                                    GameObject creepObject = Instantiate(creep.gameObject, spawnPoint.position + RandomPosition(), Quaternion.identity);
                                    creepObject.GetComponent<Creep>().SetLane(lane);
                                    //creepObject.GetComponent<Creep>().SetUpgradeCount(upgradeCount);


                                }


                            }
                        }
                        break;
                    }
            }


            switch (rangeDestroyed)
            {
                case false:
                    {
                        if (creep.GetCreepType() == CreepType.CreepTypes.Range && creep.GetCreepPower() == CreepPower.CreepPowers.Normal)
                        {
                            for (int i = 0; i < rangeCreepCount; i++)
                            {
                                if (upgradeCount <= 0)
                                {
                                    GameObject creepObject = Instantiate(creep.gameObject, spawnPoint.position + RandomPosition(), Quaternion.identity);
                                    creepObject.GetComponent<Creep>().SetLane(lane);


                                }
                                else
                                {
                                    creep.SetUpgradeCount(upgradeCount);
                                    GameObject creepObject = Instantiate(creep.gameObject, spawnPoint.position + RandomPosition(), Quaternion.identity);
                                    creepObject.GetComponent<Creep>().SetLane(lane);
                                    //creepObject.GetComponent<Creep>().SetUpgradeCount(upgradeCount);

                                }

                            }
                        }
                        break;
                    }
                case true:
                    {
                        if (creep.GetCreepType() == CreepType.CreepTypes.Range && creep.GetCreepPower() == CreepPower.CreepPowers.Super)
                        {
                            for (int i = 0; i < rangeCreepCount; i++)
                            {
                                if (upgradeCount <= 0)
                                {
                                    GameObject creepObject = Instantiate(creep.gameObject, spawnPoint.position + RandomPosition(), Quaternion.identity);
                                    creepObject.GetComponent<Creep>().SetLane(lane);

                                }
                                else
                                {
                                    creep.SetUpgradeCount(upgradeCount);
                                    GameObject creepObject = Instantiate(creep.gameObject, spawnPoint.position + RandomPosition(), Quaternion.identity);
                                    creepObject.GetComponent<Creep>().SetLane(lane);
                                    //creepObject.GetComponent<Creep>().SetUpgradeCount(upgradeCount);

                                }

                            }
                        }
                        break;
                    }
            }
            

            if(waveCount % 10 == 0)
            {
                switch (meleeDestroyed && rangeDestroyed)
                {
                    case false:
                        {
                            if (creep.GetCreepType() == CreepType.CreepTypes.Siege && creep.GetCreepPower() == CreepPower.CreepPowers.Normal)
                            {
                                if (upgradeCount <= 0)
                                {
                                    GameObject creepObject = Instantiate(creep.gameObject, spawnPoint.position + RandomPosition(), Quaternion.identity);
                                    creepObject.GetComponent<Creep>().SetLane(lane);
                                }
                                else
                                {
                                    creep.SetUpgradeCount(upgradeCount);
                                    GameObject creepObject = Instantiate(creep.gameObject, spawnPoint.position + RandomPosition(), Quaternion.identity);
                                    creepObject.GetComponent<Creep>().SetLane(lane);

                                }
                            }
                            break;
                        }

                    case true:
                        {
                            if (creep.GetCreepType() == CreepType.CreepTypes.Siege && creep.GetCreepPower() == CreepPower.CreepPowers.Super)
                            {
                                if (upgradeCount <= 0)
                                {
                                    GameObject creepObject = Instantiate(creep.gameObject, spawnPoint.position + RandomPosition(), Quaternion.identity);
                                    creepObject.GetComponent<Creep>().SetLane(lane);
                                }
                                else
                                {
                                    creep.SetUpgradeCount(upgradeCount);
                                    GameObject creepObject = Instantiate(creep.gameObject, spawnPoint.position + RandomPosition(), Quaternion.identity);
                                    creepObject.GetComponent<Creep>().SetLane(lane);


                                }
                            }
                            break;
                        }
                }
               
            }          
        }
        Debug.Log("Spawn Creep");
        
    }

    private Vector3 RandomPosition()
    {
        float x = UnityEngine.Random.Range(5f, 10f);
        float y = 1f;
        float z = UnityEngine.Random.Range(5f, 10f);

        return new Vector3(x, y, z); 
    }
    
    public void IncreaseUpgradeCount()
    {
        upgradeCount++;
    }

    public void CheckBuildings(Buildings building)
    {
        foreach(var barrack in barracksList)
        {
            if(barrack == building)
            {
                if(barrack.GetBuildingType() == BuildingType.BuildingTypes.MeleeBarracks)
                {
                    meleeDestroyed = true;
                }

                if(barrack.GetBuildingType() == BuildingType.BuildingTypes.RangeBarracks)
                {
                    rangeDestroyed = true;
                }
            }
        }
    }

    public SpawnerLane.SpawnLane GetLane()
    {
        return lane;
    }

    
}
