﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraBoundsManager : MonoBehaviour
{
    
    [SerializeField] private CinemachineVirtualCamera cinemachineVirtualCamera;
    private Transform cameraTransform;


    //Camera Bounds//
    [Tooltip("Camera Bounds offset")]
    [SerializeField] private float offsetMin = 50f;
    [SerializeField] private float offsetMax = 50f;


    private Terrain worldTerrain;
    private float terrainWidth;
    private float terrainLength;
    private float cameraBoundsMinWidth;
    private float cameraBoundsMaxWidth;
    private float cameraBoundsMinLength;
    private float cameraBoundsMaxLength;

    private void Awake()
    {
        worldTerrain = FindObjectOfType<Terrain>();

        terrainWidth = worldTerrain.terrainData.size.x;
        terrainLength = worldTerrain.terrainData.size.z;
        cameraBoundsMinWidth = 0 + offsetMin;
        cameraBoundsMaxWidth = terrainWidth - offsetMax;
        cameraBoundsMinLength = 0 + offsetMin;
        cameraBoundsMaxLength = terrainLength - offsetMax;

        cameraTransform = cinemachineVirtualCamera.VirtualCameraGameObject.transform;
    }

    // Start is called before the first frame update
    void Start()
    {
        if(cinemachineVirtualCamera == null)
        {
            Debug.Log("VirtualCamera is missing pls check if you referenced it..");
        }
    }

    // Update is called once per frame
    void Update()
    {
        CameraBoundsChecker();
    }


    //this function checks the max and min bounds of the camera depending on the terrain's width and length
    private void CameraBoundsChecker()
    {
        float x = cameraTransform.position.x;
        float z = cameraTransform.position.z;
        x = Mathf.Clamp(cameraTransform.position.x, cameraBoundsMinWidth, cameraBoundsMaxWidth);
        z = Mathf.Clamp(cameraTransform.position.z, cameraBoundsMinLength, cameraBoundsMaxLength);

        cameraTransform.position = new Vector3(x, cameraTransform.position.y, z);
   

        
    }
}
