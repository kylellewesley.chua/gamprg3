﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;



public class DayNightCycleHandler : MonoBehaviour
{
   
    public Color day;
    public Color night;

    [SerializeField] Light globalLight;

    [Tooltip("Debugging Purpose")]
    [SerializeField] private bool isDay = false;
    

    // Start is called before the first frame update
    void Start()
    {
        
        if (globalLight == null)
        {
            Debug.Log("Please Reference Directional Light");
        }

        TimerManager timerManager = SingletonManager.Get<TimerManager>();
        timerManager.FiveMinuteMark(5, 0, () =>
        {
            Debug.Log("5 mins");
            if (isDay)
            {
                globalLight.color = night;
                isDay = false;
            }
            else
            {
                globalLight.color = day;
                isDay = true;
            }
                
        });
        
       
       
    }

   

    // Update is called once per frame
    void Update()
    {
        
    }

  
}
