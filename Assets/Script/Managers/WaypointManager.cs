﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointManager : MonoBehaviour
{
    [Header("Radiant Mid Lane Waypoint")]
    [SerializeField] List<Waypoints> midLaneWaypointList_R;

    [Header("Radiant Top Lane Waypoint")]
    [SerializeField] List<Waypoints> topLaneWaypointList_R;

    [Header("Radiant Bot Lane Waypoint")]
    [SerializeField] List<Waypoints> botLaneWaypointList_R;

    [Header("-----------------------------")]

    [Header("Radiant Mid Lane Waypoint")]
    [SerializeField] List<Waypoints> midLaneWaypointList_D;

    [Header("Radiant Top Lane Waypoint")]
    [SerializeField] List<Waypoints> topLaneWaypointList_D;

    [Header("Radiant Bot Lane Waypoint")]
    [SerializeField] List<Waypoints> botLaneWaypointList_D;


    // Start is called before the first frame update
    void Start()
    {
        SingletonManager.Register<WaypointManager>(this);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public List<Waypoints> GetRadiantMidWaypoints()
    {
        return midLaneWaypointList_R;
    }

    public List<Waypoints> GetRadiantTopWaypoints()
    {
        return topLaneWaypointList_R;
    }

    public List<Waypoints> GetRadiantBotWaypoints()
    {
        return botLaneWaypointList_R;
    }

    public List<Waypoints> GetDireMidWaypoints()
    {
        return midLaneWaypointList_D;
    }

    public List<Waypoints> GetDireTopWaypoints()
    {
        return topLaneWaypointList_D;
    }

    public List<Waypoints> GetDireBotWaypoints()
    {
        return botLaneWaypointList_D;
    }
}
