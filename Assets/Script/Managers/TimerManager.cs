﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Events;

[System.Serializable]
public class SpawnEvent : UnityEvent
{

}

[System.Serializable]
public class UpgradeStatEvent : UnityEvent { }

public class TimerManager : MonoBehaviour
{
   
    public SpawnEvent OnThirtySecondsEvents;

    public UpgradeStatEvent OnSevenThirtyEvents;


    //public class OnThirtySecondsArgs : EventArgs
    //{
    //    public bool spawn;
    //}
    //public event EventHandler<OnThirtySecondsArgs> OnThirtySeconds;


    private Action fiveMinutesCallBack;
    
    

    private float timer;
    [SerializeField] private float t;

    private float seconds;
    private float minutes;
    private float hour;

    private bool fiveMinsActionCalled = false;
    private bool thirtyMinsActionCalled = false;
    [SerializeField] private bool sevenThirtyActionCalled = false;

    

    [SerializeField] float timerTest;

    private void Awake()
    {
        
        SingletonManager.Register<TimerManager>(this);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        GameTimer();
    }

    private void GameTimer()
    {
        t = Time.time - timer + timerTest;
        seconds = ((int)t % 60);
        minutes = ((int)t / 60) % 60;
        hour = ((int)t / 3600);

        if (IsThirtySeconds(seconds))
        {
            if (!thirtyMinsActionCalled)
            {
                OnThirtySecondsEvents?.Invoke();
                //OnThirtySeconds?.Invoke(this, new OnThirtySecondsArgs { spawn = true });
                thirtyMinsActionCalled = true;
            }
           
        }

        if (IsFiveMinutes(minutes, seconds))
        {
            if (!fiveMinsActionCalled)
            {
                fiveMinutesCallBack();
                fiveMinsActionCalled = true;
            }
           
        }

        if (IsSevenThirtyMinutes(t))
        {
            if (!sevenThirtyActionCalled)
            {
                Debug.Log("Invoke Upgrade");
                StartCoroutine(SevenThirtyMinutes());
            }
        }

       

        //just Set Action call back to false so the callback can run again the next 5 min
        if(seconds == 1)
        {
            SetActionCalled(false);
        }

        if(seconds == 1 || seconds == 31)
        {
            thirtyMinsActionCalled = false;
        }

        //Debug.Log(hour + ":" + minutes + ":" + seconds);
    }

    public void FiveMinuteMark(float minutes, float seconds, Action fiveMinutesCallback)
    {
        this.fiveMinutesCallBack = fiveMinutesCallback;    
    }

    public bool IsFiveMinutes(float minutes, float seconds)
    {
        return minutes % 5 == 0 && seconds == 0;
    }

    public bool IsThirtySeconds(float seconds)
    {
        return seconds == 30 || seconds == 0;
    }

    public bool IsSevenThirtyMinutes(float timeInSeconds)
    {
        return (int)timeInSeconds % 449 == 0 && (int)timeInSeconds != 0;
    }

   

    public void SetActionCalled(bool value)
    {
        fiveMinsActionCalled = value;
    }



    public void TimerDebug(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            timerTest += 445;
        }
       
    }

    private IEnumerator SevenThirtyMinutes()
    {
        OnSevenThirtyEvents?.Invoke();
        sevenThirtyActionCalled = true;
        yield return new WaitForSeconds(1f);
        sevenThirtyActionCalled = false;

    }

   


}
