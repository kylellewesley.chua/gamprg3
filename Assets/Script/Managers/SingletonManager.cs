﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;

public static class SingletonManager
{
    private static Dictionary<System.Type, MonoBehaviour> singletons = new Dictionary<System.Type, MonoBehaviour>();

    public static T Get<T>() where T : MonoBehaviour
    {
        System.Type type = typeof(T);
        return singletons[typeof(T)] as T;
    }

    public static void Register<T>(T obj) where T : MonoBehaviour
    {
        singletons[typeof(T)] = obj;

    }

    public static void Remove<T>() where T : MonoBehaviour
    {
        singletons.Remove(typeof(T));
    }
        

}
