﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class ShoudSpawnEvent : UnityEvent { }

[System.Serializable]
public class ShouldUpgradeEvent : UnityEvent { }

public class SpawnManager : MonoBehaviour
{
    //public class OnShouldSpawnArgs : EventArgs
    //{
    //    public bool shouldSpawn;
    //}

    //public event EventHandler<OnShouldSpawnArgs> OnShouldSpawn;

    public ShoudSpawnEvent OnShouldSpawnEvents;
    public ShouldUpgradeEvent OnShouldUpgradeEvent;


    [Header("Radiant Spawner")]
    [SerializeField] private List<CreepSpawner> spawner_R;
   

    [Header("DireSpawner")]
    [SerializeField] private List<CreepSpawner> spawner_D;
   

    private void Awake()
    {
        SingletonManager.Register<SpawnManager>(this);
    }

    // Start is called before the first frame update
    void Start()
    {
        TimerManager timeManager = SingletonManager.Get<TimerManager>();
        //timeManager.OnThirtySeconds += TimeManager_OnThirtySeconds;
        //timeManager.OnThirtySecondsEvents.AddListener(ThirtySecondSpawn);
    }

    //private void TimeManager_OnThirtySeconds(object sender, TimerManager.OnThirtySecondsArgs e)
    //{
    //    OnShouldSpawn?.Invoke(this, new OnShouldSpawnArgs { shouldSpawn = e.spawn });
    //}

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ThirtySecondSpawn()
    {
        
        OnShouldSpawnEvents?.Invoke();
    }

    public void UpgradeCreepStats()
    {
        
        OnShouldUpgradeEvent?.Invoke();
    }



   
}
