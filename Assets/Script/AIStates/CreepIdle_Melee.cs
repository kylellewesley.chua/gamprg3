﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreepIdle_Melee : CharacterStateBase
{
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        switch (GetAIController(animator).GetState())
        {
            case AIState.State.Idle:
                animator.SetBool("IsMoving", false);
                if (GetAIController(animator).GetWaypoints() != null)
                {
                    GetAIController(animator).SetState(AIState.State.Running);
                }

               
                break;

            case AIState.State.Running:
                animator.SetBool("IsMoving", true);
                break;

        }
                
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        
    }

   
}
