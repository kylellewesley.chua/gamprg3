﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreepAttacking_Melee : CharacterStateBase
{
    float distance;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        GetAIController(animator).GetAgent().isStopped = true;
        GetAIController(animator).GetAnimator().SetBool("IsAttacking", true);
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (GetAIController(animator).GetTargetTransform())
        {
            distance = Vector3.Distance(GetAIController(animator).GetTargetTransform().position, GetAIController(animator).transform.position);
        }
        


        if (GetAIController(animator).hasTarget && distance < 4f)
        {
            GetAIController(animator).SetState(AIState.State.Attack);

        }
        else
        {
            GetAIController(animator).SetState(AIState.State.Running);
           
        }

        switch (GetAIController(animator).GetState())
        {
            case AIState.State.Attack:
                animator.SetBool("IsAttacking", true);
                animator.SetBool("IsMoving", false);
                break;

            case AIState.State.Running:
                animator.SetBool("IsAttacking", false);
                animator.SetBool("IsMoving", true);
                break;
        }



    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        GetAIController(animator).GetAnimator().SetBool("IsAttacking", false);
    }
}
