﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterStateBase : StateMachineBehaviour
{
    private AIController controller;
    public AIController GetAIController(Animator animator)
    {
        if(controller == null)
        {
            controller = animator.GetComponentInParent<AIController>();
        }
        return controller;
    }
}
