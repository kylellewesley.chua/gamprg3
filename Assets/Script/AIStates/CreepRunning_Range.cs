﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreepRunning_Range : CharacterStateBase
{
    [SerializeField] private int currentIndex = 0;
    [SerializeField] private float speed;
    [SerializeField] private float distance;
    [SerializeField] private Waypoints waypoint;
    
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        GetAIController(animator).GetAgent().speed = speed;
        GetAIController(animator).GetAgent().isStopped = false;

        GetAIController(animator).GetAnimator().SetTrigger("IsMoving");
        
      

    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (!GetAIController(animator).hasTarget)
        {
            GetAIController(animator).GetAgent().SetDestination(GetAIController(animator).GetWaypoints()[currentIndex].transform.position);
            waypoint = GetAIController(animator).GetWaypoints()[currentIndex];

            distance = Vector3.Distance(waypoint.transform.position, GetAIController(animator).transform.position);

            if (currentIndex >= GetAIController(animator).GetWaypoints().Count - 1)
            {
                animator.SetBool("IsMoving", false);

            }

            if (distance < 5f)
            {
                currentIndex++;

            }

          
        }
        else
        {
            GetAIController(animator).GetAgent().SetDestination(GetAIController(animator).GetTarget().transform.position);
            GetAIController(animator).GetAgent().stoppingDistance = 20f;

            if (GetAIController(animator).GetAgent().remainingDistance < 40f)
            {
                
                GetAIController(animator).SetState(AIState.State.Attack);
                animator.SetBool("IsAttacking", true);
                animator.SetBool("IsMoving", false);

            }
        }

    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //if (!GetAIController(animator).hasTarget)
        //{
        //    currentIndex++;
        //}
        GetAIController(animator).GetAnimator().SetTrigger("IsMoving");
    }
}
