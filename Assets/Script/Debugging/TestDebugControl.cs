﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Events;

[System.Serializable]
public class OnDamageEvent : UnityEvent<float, GameObject> { }


[System.Serializable]
public class OnLevelUpEvent : UnityEvent { }

public class TestDebugControl : MonoBehaviour
{
    public OnDamageEvent OnDamage;
    public OnLevelUpEvent OnLevelUp;

    public float damage = 20f;
    public float bountyEarned = 0;
    public float expEarned = 0;
   
    


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void TestDamage(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            Debug.Log("Damage Creep");

            OnDamage?.Invoke(damage,this.gameObject);
        }
       
        
         
    }

    public void TestLevelUp(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            Debug.Log("Level Up");
            OnLevelUp?.Invoke();

        }
    }


    
}
