﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Events;

public class InputHandler : MonoBehaviour
{
    [SerializeField] Players.PlayerNumber playerNum;
    [SerializeField] PlayerController playerControl;

    [SerializeField] Transform testMoveLocation;
    [SerializeField] LayerMask layer;
    [SerializeField] LayerMask targetLayer;

    private MouseControls controls;

    private void Awake()
    {
        SingletonManager.Register<InputHandler>(this);
        controls = new MouseControls();
    }

    private void OnEnable()
    {
        controls.Enable();
    }

    private void OnDisable()
    {
        controls.Disable();
    }

    // Start is called before the first frame update
    void Start()
    {

    }


    // Update is called once per frame
    void Update()
    {
       
    }

    public MouseControls GetControl()
    {
        return controls;
    }

    public PlayerController GetPlayerControl()
    {
        return playerControl;
    }

    public void StopUnit(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            Debug.Log(playerControl.hasControl);
            if (playerControl.hasControl)
            {
                playerControl.SetState(PlayerState.State.Idle);
                playerControl.hasTarget = false;
                playerControl.SetTargetTransformToNull();
                Debug.Log("StopUnit");
            }
        }
    }


    public void MoveUnit(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            if (playerControl.hasControl)
            {
                if (playerControl.isTargetingSpell)
                {
                    playerControl.isTargetingSpell = false;
                }
                var ray = Camera.main.ScreenPointToRay(controls.UnitControl.MousePosition.ReadValue<Vector2>());
                RaycastHit hit;


                if (Physics.Raycast(ray, out hit) && hit.transform.tag == "Dire")
                {
                    Debug.Log(hit.transform.tag);
                    playerControl.moveLocation = hit.transform.position;
                    testMoveLocation.position = playerControl.moveLocation;
                    playerControl.SetState(PlayerState.State.Running);
                    playerControl.SetTarget(hit.transform);
                    Debug.Log("Target: " + hit.transform);
                    playerControl.hasTarget = true;

                }

                if (Physics.Raycast(ray, out hit) && hit.transform.tag == "Grounded")
                {
                    Debug.Log(hit.transform.tag);
                    playerControl.moveLocation = hit.point;
                    testMoveLocation.position = playerControl.moveLocation;
                    playerControl.SetState(PlayerState.State.Running);
                    playerControl.hasTarget = false;
                    playerControl.SetTargetTransformToNull();
                   
                }

               
                
               
            }
        }
        
    }


    public void ControlUnit(InputAction.CallbackContext context)
    {
        if (context.performed && !playerControl.isTargetingSpell)
        {
            var ray = Camera.main.ScreenPointToRay(controls.UnitControl.MousePosition.ReadValue<Vector2>());
            RaycastHit hit;
            if(Physics.Raycast(ray, out hit))
            {
                var selection = hit.transform;
                if (selection.GetComponent<PlayerController>())
                {
                    var checkPlayer = selection.GetComponent<PlayerController>();
                    if (checkPlayer.GetPlayerNum() == playerNum)
                    {
                        checkPlayer.hasControl = true;
                        Debug.Log("Player has Control: " + checkPlayer.hasControl);
                    }
                    else
                    {
                        checkPlayer.hasControl = false;
                        Debug.Log("Player has Control: " + checkPlayer.hasControl);
                    }
                }
                else
                {
                    playerControl.hasControl = false;
                    Debug.Log("Player has Control: " + playerControl.hasControl);
                }
            }
        }
    }
}


