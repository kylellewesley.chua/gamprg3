﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerState
{
    public enum State
    {
        Idle,
        Running,
        Attack
    };
}

public class PlayerController : MonoBehaviour
{
    [SerializeField] private PlayerState.State state;
    [SerializeField] private Players.PlayerNumber playerNum;
    [SerializeField] private FactionType.FactionTypes faction;
    [SerializeField] private NavMeshAgent agent;
    [SerializeField] private Animator anim;


    
    [SerializeField] private Transform targetTransform;
    [SerializeField] private float targetMaxRadius = 20f;
    public Vector3 moveLocation { set; get; }
    public bool hasTarget;
    public bool hasControl { set; get; }
    public bool isTargetingSpell { set; get; }
    [SerializeField] private float turnRate;

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        moveLocation = transform.position;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if(state == PlayerState.State.Idle)
        {
            CheckForTarget(targetTransform);
            if (targetTransform == null)
            {
                LookForTarget();
            }
        }

    }

    public void CheckForTarget(Transform target)
    {
        if (target != null)
        {
            hasTarget = true;
            Vector3 direction = target.position - transform.position;
            Quaternion rotation = Quaternion.LookRotation(direction);
            transform.rotation = Quaternion.Lerp(transform.rotation, rotation, turnRate * Time.deltaTime);
        }
        else
        {
            hasTarget = false;
            LookForTarget();
        }
    }



    public void LookForTarget()
    {

        Collider[] colliderArray = Physics.OverlapSphere(transform.position, targetMaxRadius);


        foreach (Collider collider in colliderArray)
        {
            PlayerController enemyPlayer = collider.GetComponent<PlayerController>();
            Creep creep = collider.GetComponent<Creep>();
            Buildings building = collider.GetComponent<Buildings>();

            CheckHeroesNearby(enemyPlayer);
            CheckCreepsNearby(creep);
            CheckBuildingsNearby(building);

        }
    }


   

    private void CheckBuildingsNearby(Buildings building)
    {
        if (building != null)
        {
            if (building.GetFactionType() != this.gameObject.GetComponent<PlayerController>().GetFaction())
            {
                if (targetTransform == null)
                {
                    if (building.GetIsVunerable())
                    {
                        targetTransform = building.transform;
                    }

                }
                else
                {
                    //change to the closest target
                    if (Vector3.Distance(transform.position, building.transform.position) <
                    Vector3.Distance(transform.position, targetTransform.position))
                    {
                        targetTransform = building.transform;
                        Debug.Log("Building located: " + targetTransform);
                    }
                }
            }
        }
    }

    private void CheckCreepsNearby(Creep creep)
    {
        if (creep != null)
        {
            //Check if they are not on the same faction
            if (creep.GetFaction() != this.gameObject.GetComponent<PlayerController>().GetFaction())
            {
                if (targetTransform == null)
                {
                    targetTransform = creep.transform;
                }
                else
                {
                    //change to the closest target
                    if (Vector3.Distance(transform.position, creep.transform.position) <
                        Vector3.Distance(transform.position, targetTransform.position))
                    {
                        targetTransform = creep.transform;
                    }
                }
            }
        }
    }


    private void CheckHeroesNearby(PlayerController enemyPlayer)
    {
        if (enemyPlayer != null)
        {
            //Check if they are not on the same faction
            if (enemyPlayer.GetFaction() != this.gameObject.GetComponent<PlayerController>().GetFaction())
            {
                if (targetTransform == null)
                {
                    targetTransform = enemyPlayer.transform;
                }
                else
                {
                    //change to the closest target
                    if (Vector3.Distance(transform.position, enemyPlayer.transform.position) <
                        Vector3.Distance(transform.position, targetTransform.position))
                    {
                        targetTransform = enemyPlayer.transform;
                    }
                }
            }
        }
    }

    public void LookAtLocationLerp()
    {
        Vector3 direction = GoToTargetOrLocation();

        Quaternion rotation = Quaternion.LookRotation(direction);
        transform.rotation = Quaternion.Lerp(transform.rotation, rotation, turnRate * Time.deltaTime);
    }

    private Vector3 GoToTargetOrLocation()
    {
        Vector3 direction;
        if (targetTransform != null)
        {
            direction = targetTransform.position - transform.position;
        }
        else
        {
            direction = moveLocation - transform.position;
        }

        return direction;
    }

    public void IsLookingAtLocation()
    {
        //Vector3 direction = moveLocation - transform.position;
        //Quaternion rotation = Quaternion.LookRotation(direction);
        //transform.rotation = Quaternion.Lerp(transform.rotation, rotation, turnRate * Time.deltaTime);

        Vector3 targetPosition = new Vector3(moveLocation.x, transform.position.y, moveLocation.z);
        transform.LookAt(targetPosition);
    }

    public bool CheckDistance()
    {
        float distance = TargetDistanceORLocationDistance();
      
        if (distance > .9)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public float TargetDistanceORLocationDistance()
    {
        float distance;
        if (targetTransform != null)
        {
            distance = Vector3.Distance(targetTransform.position, transform.position);
        }
        else
        {
            distance = Vector3.Distance(moveLocation, transform.position);
        }

        return distance;
    }

    public Players.PlayerNumber GetPlayerNum()
    {
        return playerNum;
    }

    public FactionType.FactionTypes GetFaction()
    {
        return faction;
    }

    public NavMeshAgent GetAgent()
    {
        return agent;
    }

    public Animator GetAnimator()
    {
        return anim;
    }

    public PlayerState.State GetState()
    {
        return state; 
    }

    public void SetState(PlayerState.State value)
    {
        state = value;
    }

    public Transform GetTarget()
    {
        return targetTransform;
    }
    
    public void SetTarget(Transform value)
    {
        targetTransform = value;
    }

    public void SetTargetTransformToNull()
    {
        targetTransform = null;
    }





}

[System.Serializable]
public class Players 
{
    public enum PlayerNumber
    {
        Player1,
        Player2,
        Player3
    };
}
