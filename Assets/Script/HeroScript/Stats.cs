﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Stats : MonoBehaviour
{
    public UnityEvent OnLevelUp;

    [SerializeField] private HeroStats myHero;
    [SerializeField] private float heroLevel;
    [SerializeField] private float strength;
    [SerializeField] private float agility;
    [SerializeField] private float intelligence;
    [SerializeField] private float health;
    [SerializeField] private float healthMax;
    [SerializeField] private float hpRegen;
    [SerializeField] private float mana;
    [SerializeField] private float manaMax;
    [SerializeField] private float mpRegen;
    [SerializeField] private float armor;
    [SerializeField] private float moveSpeed;
    [SerializeField] private float magicResistance;
    [SerializeField] private float damage;
    [SerializeField] private float attackSpeed;
    [SerializeField] private float attackRange;
    [SerializeField] private float projectileSpeed;
    [SerializeField] private float baseAttackTime;
    [SerializeField] private float spellAmp;

    [SerializeField] private float bountyReward;
    [SerializeField] private float expReward;


    public float MyStrength { get { return strength; } private set { MyStrength = value; } }
    public float MyAgility { get { return agility; } private set { MyAgility = value; } }
    public float MyIntelligence { get { return intelligence; } private set { MyIntelligence = value; } }
    public float MyHealth { get { return health; } private set { MyHealth = value; } }    
    public float MyHealthMax { get { return healthMax;  } private set { MyHealthMax = value; } }
    public float MyHpRegen { get { return hpRegen; } private set { MyHpRegen = value; } }
    public float MyMana { get { return mana; } private set { MyMana = value; } }
    public float MyManaMax { get { return manaMax; } private set { MyManaMax = value; } }
    public float MyMpRegen { get { return mpRegen; } private set { MyMpRegen = value; } }
    public float MyArmor { get { return armor; } private set { MyArmor = value; } }
    public float MyMoveSpeed { get { return moveSpeed; } private set { MyMoveSpeed = value; } }
    public float MyMagicResistance { get { return magicResistance; } private set { MyMagicResistance = value; } }
    public float MyDamage { get { return damage; } private set {MyDamage = value; } }
    public float MyAttackSpeed { get { return attackSpeed; } private set { MyAttackSpeed = value; } }
    public float MyAttackRange { get { return attackRange; } private set { MyAttackRange = value; } }
    public float MyProjectileSpeed { get { return projectileSpeed; } private set { MyProjectileSpeed = value; } }
    public float MyBaseAttackTime { get { return baseAttackTime; } private set { MyBaseAttackTime = value; } }
    public float MySpellAmp { get { return spellAmp; } private set { MySpellAmp = value; } }
    public float MyBountyReward { get { return bountyReward; } private set { MyBountyReward = value; } }
    public float MyExpReward { get { return expReward; } private set { MyExpReward = value; } }


    //Stat Gain Per Str
    private float hpPerPoint = 20f;
    private float hpRegenPerPoint = 0.1f;
    private float magicResPerPoint = 0.0008f; // 0.08%

    //Stat Gain Per Agi
    private float armorPerPoint = 0.14f;
    private float attackSpeedPerPoint = 1f;
    private float moveSpeedPerPoint = 0.0005f; // 0.05%


    //Stat Gain Per Int
    private float manaPerPoint = 12f;
    private float mpRegenPerPoint = 0.05f;
    private float spellAmpPerPoint = 0.0007f; // 0.07%


    private void Awake()
    {
        strength = myHero.baseStr;
        agility = myHero.baseAgi;
        intelligence = myHero.baseInt;
        health = myHero.health + (strength * hpPerPoint);
        mana = myHero.mana + (intelligence * manaPerPoint);
        StrStatPerPoint(strength);
        AgiStatPerPoint(agility);
        IntStatPerPoint(intelligence);
    }

    // Start is called before the first frame update
    void Start()
    {
        
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void StrStatPerPoint(float str)
    {       
        healthMax = myHero.health + (str * hpPerPoint);
        hpRegen = myHero.hpRegen + (str * hpRegenPerPoint);
        magicResistance = myHero.magicResistance + (str * magicResPerPoint);
        if(myHero.primaryAtt == PrimaryAttributes.Primary.Strength)
        {
            damage = myHero.damage + (str * 1);
        }
    }

    private void AgiStatPerPoint(float agi)
    {
        armor = myHero.armor + (agi * armorPerPoint);
        attackSpeed = myHero.attackSpeed + (agi * attackSpeedPerPoint);
        moveSpeed = myHero.moveSpeed + (agi * moveSpeedPerPoint); //Check
        if(myHero.primaryAtt == PrimaryAttributes.Primary.Agility)
        {
            damage = myHero.damage + (agi * 1);
        }
    }

    private void IntStatPerPoint(float intelligence)
    {
        manaMax = myHero.mana + (intelligence * manaPerPoint);
        mpRegen = myHero.mpRegen + (intelligence * mpRegenPerPoint);
        spellAmp = (intelligence * spellAmpPerPoint);

        if(myHero.primaryAtt == PrimaryAttributes.Primary.Intelligence)
        {
            damage = myHero.damage + (intelligence * 1);
        }
    }

    private void StatPerLevelUp()
    {
        strength += (myHero.strGrowth);
        agility += (myHero.agiGrowth);
        intelligence += (myHero.intGrowth);
        StrStatPerPoint(strength);
        AgiStatPerPoint(agility);
        IntStatPerPoint(intelligence);
    }

    public void LevelUpHero(float level)
    {
        heroLevel += level;
        StatPerLevelUp();
        OnLevelUp?.Invoke();
        
    }
}
