﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillHandler : MonoBehaviour
{
    [SerializeField] private List<Skill> skillList;
    [SerializeField] private LayerMask mask;
    [SerializeField] private Skill skillSelected;

    private InputHandler inputHandler;

    private void Awake()
    {
        foreach(var skills in skillList)
        {
            skills.isUsable = true;
        }
    }


    // Start is called before the first frame update
    void Start()
    {
        inputHandler = SingletonManager.Get<InputHandler>();
        inputHandler.GetControl().UnitControl._1stSkill.performed += ctx => { SelectSkill(0); };
        inputHandler.GetControl().UnitControl._2ndSkill.performed += ctx => { SelectSkill(1); };
        inputHandler.GetControl().UnitControl._3rdSkill.performed += ctx => { SelectSkill(2); };
        inputHandler.GetControl().UnitControl._4thSkill.performed += ctx => { SelectSkill(3); };
        inputHandler.GetControl().UnitControl.ControlUnit.performed += ctx => { SkillActivate(); };
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void LevelUpSkill(int index)
    {

    }

    public void SelectSkill(int index)
    {
        
        if (skillList[index].isUsable && inputHandler.GetPlayerControl().hasControl)
        {
            Debug.Log("Select Skill");
            skillSelected = skillList[index];
            skillList[index].OnPress(this);
        }
            
    }

    public void SkillActivate()
    {
        
        if(skillSelected != null)
        {
            if (inputHandler.GetPlayerControl().isTargetingSpell)
            {
                if(GetComponent<Mana>().GetCurrentMana() > skillSelected.manaCost[skillSelected.skillLevel])
                {
                    Debug.Log("Use Skill");
                    var ray = Camera.main.ScreenPointToRay(inputHandler.GetControl().UnitControl.MousePosition.ReadValue<Vector2>());
                    RaycastHit hit;

                    if (Physics.Raycast(ray, out hit, 1000f, skillSelected.mask))
                    {
                        skillSelected.isUsable = false;
                        StartCoolDown();
                        skillSelected.OnExecute(this, hit.collider.gameObject, hit.transform.position);
                        
                    }
                }
                //No mana feedback        
            }



        }
    }

    private void StartCoolDown()
    {
        if (!skillSelected.isUsable)
        {
            StartCoroutine(SkillCooldown());
        }
    }

    private IEnumerator SkillCooldown()
    {
        var skillToCooldown = skillSelected;
        yield return new WaitForSeconds(skillSelected.skillCD[skillSelected.skillLevel]);
        skillToCooldown.isUsable = true;

    }

    public Skill GetSkillSelected()
    {
        return skillSelected;
    }

    public void SetSkillSelectedToNull()
    {
        skillSelected = null;
    }

}
