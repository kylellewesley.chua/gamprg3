﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveOfTerror : Skill
{
    [SerializeField] GameObject projectilePrefab;
    [SerializeField] List<float> damage;
    private SkillHandler player;


    public override void OnExecute(SkillHandler handler, GameObject enemy, Vector3 location)
    {
        GameObject projectile = Instantiate(projectilePrefab, handler.transform.position, Quaternion.identity);
        projectile.GetComponent<WaveOfTerrorProjectile>().SetUser(player.transform);
        projectile.GetComponent<WaveOfTerrorProjectile>().SetTargetLocation(location);
        projectile.GetComponent<WaveOfTerrorProjectile>().OnTrigger.AddListener(HitSkill);
        player.GetComponent<PlayerController>().isTargetingSpell = false;
        handler.GetComponent<Mana>().ManaUsage(manaCost[skillLevel]);
        handler.SetSkillSelectedToNull();
    }

    public override void OnPress(SkillHandler handler)
    {
        player = handler;
        player.GetComponent<PlayerController>().isTargetingSpell = true;
    }


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void HitSkill(GameObject enemy)
    {
        enemy.GetComponent<Health>().OnDamage(this.damage[skillLevel - 1], player.gameObject);
    }
}
