﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetherSwap : Skill
{

    private SkillHandler player;

    public override void OnExecute(SkillHandler handler, GameObject enemy, Vector3 location)
    {
        var playerPosition = player.transform.position;
        var enemyPosition = enemy.transform.position;

        player.transform.position = enemyPosition;
        enemy.transform.position = playerPosition;
        player.GetComponent<PlayerController>().isTargetingSpell = false;
        handler.GetComponent<Mana>().ManaUsage(manaCost[skillLevel]);
        handler.SetSkillSelectedToNull();
    }

    public override void OnPress(SkillHandler handler)
    {
        player = handler;
        player.GetComponent<PlayerController>().isTargetingSpell = true;
    }



    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
