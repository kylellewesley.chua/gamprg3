﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class Skill : MonoBehaviour
{
    public Image spriteUI;
    public string skillName;
    public int skillLevel;
    public LayerMask mask;
    public bool isUsable;
    public List<float> skillCD;
    public List<float> manaCost;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public abstract void OnPress(SkillHandler handler);
    public abstract void OnExecute(SkillHandler handler, GameObject enemy, Vector3 location);
    




    public class SkillTier
    {
        public enum Tier
        {
            Basic,
            Ultimate
        };
    }


    public class SkillType
    {
        public enum Type
        {
            Active,
            Passive
        };
    }

    public class SkillTarget
    {
        public enum Type
        {
            None,
            Ground,
            Single,
            AoE,
        };
    }

    public class DamageType
    {
        public enum Type
        {
            None,
            Physical,
            Magical,
            Pure
        };
    }

    public class SkillEffectTo
    {
        public enum Type
        {
            Both,
            Enemies,
            Allies
        };
    }
}
