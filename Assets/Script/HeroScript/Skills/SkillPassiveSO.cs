﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(menuName = "ScriptableObject/SkillPassiveSO")]
public class SkillPassiveSO : ScriptableObject
{
    public Image uiImage;
    public GameObject prefab;
    public string name;
    //public SkillType.Type type;
    //public SkillTarget.Type skillTargetType;
    

}
