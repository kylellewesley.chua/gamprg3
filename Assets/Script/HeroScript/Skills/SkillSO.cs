﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(menuName = "ScriptableObject/SkillSO")]
public class SkillSO : ScriptableObject
{
    public Image uiImage;
    public GameObject prefab;
    public string name;
    //public SkillType.Type skillType;
    //public SkillTarget.Type skillTargetType;
    //public DamageType.Type damageType;
    //public SkillEffectTo.Type skillEffectTo;
    public List<float> auraRadiusOrCastRadius;
    public List<float> effectDuration;
    public List<float> damage;
    public List<float> cooldown;
    public List<float> manaCost;

    
    
   
}


