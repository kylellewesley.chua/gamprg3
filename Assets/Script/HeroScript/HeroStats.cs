﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PrimaryAttributes
{
    public enum Primary
    {
        Strength,
        Agility,
        Intelligence
    };
}

public class HeroType
{
    public enum Type
    {
        Range,
        Melee
    };
}



[CreateAssetMenu(menuName = "ScriptableObject/HeroStatSO")]
public class HeroStats : ScriptableObject
{
    public PrimaryAttributes.Primary primaryAtt;
    public HeroType.Type type;
    public float baseStr;
    public float strGrowth;
    public float baseAgi;
    public float agiGrowth;
    public float baseInt;
    public float intGrowth;
    public float health;
    public float hpRegen;
    public float mana;
    public float mpRegen;
    public float moveSpeed;
    public float magicResistance;
    public float damage;
    public float armor;
    public float attackSpeed;
    public float attackRange;
    public float projectileSpeed;
    public float baseAttackTime;



    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
