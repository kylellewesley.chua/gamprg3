﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [SerializeField] private Transform bowUser;
    [SerializeField] private Transform targetEnemy;
    [SerializeField] private Transform lastKnownLocation;

    private float projectileSpeed = 20f;
    private float turn = 10f;


    public static Projectile Create(Vector3 position, Transform targetEnemy, Transform user)
    {
        Transform pfArrowProjectile = Resources.Load<Transform>("arrow");
        Transform arrowTransform = Instantiate(pfArrowProjectile, position, Quaternion.identity);

        Projectile projectile = arrowTransform.GetComponent<Projectile>();
        projectile.SetTarget(targetEnemy);
        projectile.SetBowUser(user);

        return projectile;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(bowUser == null)
        {
            Destroy(gameObject);
        }

        if(targetEnemy != null)
        {
            lastKnownLocation = targetEnemy;
        }
        
        Fire();
    }

    private void DestroyProjectile()
    {
        if(targetEnemy == null && lastKnownLocation == null)
        {
            Destroy(gameObject);
        }
    }

    private void Fire()
    {
        gameObject.GetComponent<Rigidbody>().velocity = transform.forward * projectileSpeed;

        if(targetEnemy != null)
        {
            var targetRotation = Quaternion.LookRotation(targetEnemy.position - transform.position);

            gameObject.GetComponent<Rigidbody>().MoveRotation(Quaternion.RotateTowards(transform.rotation, targetRotation, turn));
        }
        else if(lastKnownLocation != null)
        {
            var targetRotation = Quaternion.LookRotation(lastKnownLocation.position - transform.position);

            gameObject.GetComponent<Rigidbody>().MoveRotation(Quaternion.RotateTowards(transform.rotation, targetRotation, turn));
        }
        else
        {
            DestroyProjectile();
        }
        
       

    }

    public void SetBowUser(Transform user) 
    {
        bowUser = user;
    }


    private void SetTarget(Transform targetEnemy)
    {
        this.targetEnemy = targetEnemy;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(targetEnemy != null)
        {
            if (other.gameObject == targetEnemy.gameObject)
            {
                if (bowUser.GetComponentInParent<Creep>())
                {
                    other.gameObject.GetComponent<Health>().OnDamage(bowUser.GetComponentInParent<Creep>().GetDamage(), bowUser.GetComponentInParent<Creep>().gameObject);
                    Destroy(gameObject);
                }
            }
        }
        
       
    }

    
}
