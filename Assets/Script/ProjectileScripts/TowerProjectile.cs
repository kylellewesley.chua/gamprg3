﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerProjectile : MonoBehaviour
{
    [SerializeField] private Transform tower;
    [SerializeField] private Transform targetEnemy;
    [SerializeField] private Transform lastKnownLocation;

    private float projectileSpeed = 20f;
    private float turn = 10f;


    public static TowerProjectile Create(Vector3 position, Transform targetEnemy, Transform user)
    {
        Transform pfTowerProjectile = Resources.Load<Transform>("towerProjectile");
        Transform projectileTransform = Instantiate(pfTowerProjectile, position, Quaternion.identity);

        TowerProjectile projectile = projectileTransform.GetComponent<TowerProjectile>();
        projectile.SetTarget(targetEnemy);
        projectile.SetTower(user);

        return projectile;
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if(tower == null)
        {
            Destroy(gameObject);
        }

        if (targetEnemy != null)
        {
            lastKnownLocation = targetEnemy;
        }

        Fire();
    }

    private void DestroyProjectile()
    {
        if (targetEnemy == null && lastKnownLocation == null)
        {
            Destroy(gameObject);
        }
    }

    private void Fire()
    {
        gameObject.GetComponent<Rigidbody>().velocity = transform.forward * projectileSpeed;

        if (targetEnemy != null)
        {
            var targetRotation = Quaternion.LookRotation(targetEnemy.position - transform.position);

            gameObject.GetComponent<Rigidbody>().MoveRotation(Quaternion.RotateTowards(transform.rotation, targetRotation, turn));
        }
        else if (lastKnownLocation != null)
        {
            var targetRotation = Quaternion.LookRotation(lastKnownLocation.position - transform.position);

            gameObject.GetComponent<Rigidbody>().MoveRotation(Quaternion.RotateTowards(transform.rotation, targetRotation, turn));
        }
        else
        {
            DestroyProjectile();
        }



    }

    public void SetTower(Transform user)
    {
        tower = user;
    }


    private void SetTarget(Transform targetEnemy)
    {
        this.targetEnemy = targetEnemy;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (targetEnemy != null)
        {
            if (other.gameObject == targetEnemy.gameObject)
            {
                if (tower.GetComponent<Buildings>())
                {
                    other.gameObject.GetComponent<Health>().OnDamage(tower.GetComponent<Buildings>().GetDamage(), tower.GetComponent<Buildings>().gameObject);
                    Destroy(gameObject);
                }
            }
        }


    }
}
