﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;



public class WaveOfTerrorProjectile : MonoBehaviour
{
    public OnTriggerEvent OnTrigger;

    [SerializeField] private Transform user;
    private Vector3 userLocation;
    [SerializeField] private Vector3 targetLocation;
    [SerializeField] private float projectileSpeed;


    public void SetUser(Transform value)
    {
        user = value;
    }

    public void SetTargetLocation(Vector3 value)
    {
        targetLocation = value;
    }


    // Start is called before the first frame update
    void Start()
    {
        userLocation = user.position;
        var targetRotation = Quaternion.LookRotation(targetLocation - transform.position);

        transform.rotation = targetRotation;
    }

    // Update is called once per frame
    void Update()
    {
        DistanceChecker();
    }

    private void FixedUpdate()
    {
        
        Fire();
    }


    private void Fire()
    {

        

        gameObject.GetComponent<Rigidbody>().velocity = transform.forward * projectileSpeed;



    }

    private void DistanceChecker()
    {
        

        var distance = Vector3.Distance(this.gameObject.transform.position, userLocation);

        if(distance >= 45)
        {
            Destroy(gameObject);
        }

    } 



    private void OnTriggerEnter(Collider other)
    {
        if (targetLocation != null)
        {
            if (other.GetComponent<Creep>())
            {
                if (other.gameObject.GetComponent<Creep>().GetFaction() != user.GetComponent<PlayerController>().GetFaction())
                {
                    OnTrigger?.Invoke(other.gameObject);
                    //Destroy(gameObject);

                }
            }
           
        }


    }


}
