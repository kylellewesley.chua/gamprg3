﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class OnTriggerEvent : UnityEvent<GameObject>
{

}


public class MagicMissileProjectile : MonoBehaviour
{
    public OnTriggerEvent OnTrigger;

    [SerializeField] private Transform user;
    [SerializeField] private Transform targetEnemy;
    private Transform lastKnownLocation;

    [SerializeField] private float projectileSpeed;
    private float turn;



    public void SetUser(Transform value)
    {
        user = value;
    }

    public void SetTarget(Transform value)
    {
        targetEnemy = value;
    }


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        Debug.Log(targetEnemy);
        Fire();
    }


    private void Fire()
    {

       
        
        if (targetEnemy != null)
        {
            var targetRotation = Quaternion.LookRotation(targetEnemy.position - transform.position);

            transform.rotation = targetRotation;
            
        }
        else if (lastKnownLocation != null)
        {
            var targetRotation = Quaternion.LookRotation(lastKnownLocation.position - transform.position, Vector3.up);

            transform.rotation = targetRotation;

        }
        else
        {
            DestroyProjectile();
        }

        gameObject.GetComponent<Rigidbody>().velocity = transform.forward * projectileSpeed;



    }

    private void DestroyProjectile()
    {
        if (targetEnemy == null && lastKnownLocation == null)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (targetEnemy != null)
        {
            if (other.gameObject == targetEnemy.gameObject)
            {
                OnTrigger?.Invoke(other.gameObject);
                Destroy(gameObject);
                
            }
        }


    }
}
